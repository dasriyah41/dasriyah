import React from 'react';
import { Image, TextInput, ScrollView,  Text } from 'react-native';

const logo = {
  uri: 'https://reactnative.dev/img/tiny_logo.png',
  width: 64,
  height: 64.
};

const App = () => (
  <ScrollView>
    <Image source={require('./das/yp.jpg')} style={{width:420, height:40}} />
    <Text style={{ fontSize: 60 }}>Scrolling down</Text>
    <Image source={require('./das/d.png')} style={{flex:1,width:420,height:230}} />
    
    <TextInput
        style={{
          height: 40,
          borderColor: 'gray',
          borderWidth: 1
        }}
        Value="You can type in me"
      />
    
    
    <Image source={require('./das/gg.jpg')} style={{flex:1,width:420,height:300}} />
    <Image source={require('./das/gh.jpg')} style={{flex:1,width:420,height:300}}/>
    
    
    
    
    <Image source={require('./das/yb.jpg')} style={{width:420, height:40}}  />

    
    <Text style={{ fontSize: 80 }}>React Native</Text>
  </ScrollView>
);

export default App;